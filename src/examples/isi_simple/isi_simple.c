#include <px4_config.h>
#include <px4_tasks.h>
#include <px4_posix.h>
/*#include <px4_log.h>*/
#include <unistd.h>
#include <stdio.h>
#include <poll.h>
#include <string.h>
#include <math.h>


#include <uORB/topics/sensor_combined.h>
#include <uORB/topics/vehicle_attitude.h>
#include <uORB/uORB.h>

__EXPORT int isi_simple_main(int argc, char **  argv);

int isi_simple_main(int argc, char **  argv){
   struct sensor_combined_s raw;
   struct vehicle_attitude_s att;

   int sensor_sub_fd = orb_subscribe(ORB_ID(sensor_combined));
   orb_advert_t att_pub_fd = orb_advertise(ORB_ID(vehicle_attitude), &att);

   px4_pollfd_struct_t fds[] = {
      { .fd=sensor_sub_fd, .events=POLLIN},
   };

   PX4_INFO("hola mundo, estoy listo para volar");

   memset(&att,0,sizeof(att));
   while(1){
      px4_poll(fds,1 ,1000);
      if (fds[0].revents & POLLIN){
         orb_copy(ORB_ID(sensor_combined), sensor_sub_fd, &raw);
         PX4_INFO("Lectura del acelerometro:\t %8.4f \t %8.4f \t %8.4f \n",(double)raw.accelerometer_m_s2[0],
                                                                     (double)raw.accelerometer_m_s2[1],
                                                                     (double)raw.accelerometer_m_s2[2]);
      }
      att.q[0]=raw.accelerometer_m_s2[0];
      att.q[1]=raw.accelerometer_m_s2[1];
      att.q[2]=raw.accelerometer_m_s2[2];
      orb_publish(ORB_ID(vehicle_attitude),att_pub_fd,&att);
   }
   return OK;
}

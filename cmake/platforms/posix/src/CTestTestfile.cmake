# CMake generated Testfile for 
# Source directory: /home/isidro/src/Firmware/platforms/posix/src
# Build directory: /home/isidro/src/Firmware/cmake/platforms/posix/src
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs(lockstep_scheduler)
subdirs(px4_daemon)
subdirs(px4_layer)

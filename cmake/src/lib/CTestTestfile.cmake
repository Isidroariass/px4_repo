# CMake generated Testfile for 
# Source directory: /home/isidro/src/Firmware/src/lib
# Build directory: /home/isidro/src/Firmware/cmake/src/lib
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs(airspeed)
subdirs(battery)
subdirs(bezier)
subdirs(cdev)
subdirs(circuit_breaker)
subdirs(CollisionPrevention)
subdirs(controllib)
subdirs(conversion)
subdirs(drivers)
subdirs(ecl)
subdirs(FlightTasks)
subdirs(landing_slope)
subdirs(led)
subdirs(mathlib)
subdirs(mixer)
subdirs(perf)
subdirs(pid)
subdirs(pwm_limit)
subdirs(rc)
subdirs(systemlib)
subdirs(terrain_estimation)
subdirs(tunes)
subdirs(version)
subdirs(WeatherVane)

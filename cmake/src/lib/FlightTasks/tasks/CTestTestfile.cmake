# CMake generated Testfile for 
# Source directory: /home/isidro/src/Firmware/src/lib/FlightTasks/tasks
# Build directory: /home/isidro/src/Firmware/cmake/src/lib/FlightTasks/tasks
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs(FlightTask)
subdirs(Utility)
subdirs(Manual)
subdirs(Auto)
subdirs(AutoMapper)
subdirs(AutoMapper2)
subdirs(ManualAltitude)
subdirs(ManualAltitudeSmooth)
subdirs(ManualPosition)
subdirs(ManualPositionSmooth)
subdirs(ManualPositionSmoothVel)
subdirs(Sport)
subdirs(AutoLine)
subdirs(AutoLineSmoothVel)
subdirs(AutoFollowMe)
subdirs(Offboard)
subdirs(Failsafe)
subdirs(Transition)
subdirs(Orbit)

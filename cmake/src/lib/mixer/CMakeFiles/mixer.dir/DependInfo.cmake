# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/home/isidro/src/Firmware/src/lib/mixer/mixer_load.c" "/home/isidro/src/Firmware/cmake/src/lib/mixer/CMakeFiles/mixer.dir/mixer_load.c.o"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_C
  "CONFIG_ARCH_BOARD_PX4_SITL"
  "ENABLE_LOCKSTEP_SCHEDULER"
  "__CUSTOM_FILE_IO__"
  "__DF_LINUX"
  "__PX4_LINUX"
  "__PX4_POSIX"
  "__STDC_FORMAT_MACROS"
  "noreturn_function=__attribute__((noreturn))"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "../boards/px4/sitl/src"
  "."
  "src"
  "src/lib"
  "src/modules"
  "../src"
  "../src/include"
  "../src/lib"
  "../src/lib/DriverFramework/framework/include"
  "../src/lib/matrix"
  "../src/modules"
  "../src/platforms"
  "../src/platforms/common"
  "../platforms/posix/include"
  "external/Install/include"
  "src/lib/mixer"
  )
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/isidro/src/Firmware/src/lib/mixer/mixer.cpp" "/home/isidro/src/Firmware/cmake/src/lib/mixer/CMakeFiles/mixer.dir/mixer.cpp.o"
  "/home/isidro/src/Firmware/src/lib/mixer/mixer_group.cpp" "/home/isidro/src/Firmware/cmake/src/lib/mixer/CMakeFiles/mixer.dir/mixer_group.cpp.o"
  "/home/isidro/src/Firmware/src/lib/mixer/mixer_helicopter.cpp" "/home/isidro/src/Firmware/cmake/src/lib/mixer/CMakeFiles/mixer.dir/mixer_helicopter.cpp.o"
  "/home/isidro/src/Firmware/src/lib/mixer/mixer_multirotor.cpp" "/home/isidro/src/Firmware/cmake/src/lib/mixer/CMakeFiles/mixer.dir/mixer_multirotor.cpp.o"
  "/home/isidro/src/Firmware/src/lib/mixer/mixer_simple.cpp" "/home/isidro/src/Firmware/cmake/src/lib/mixer/CMakeFiles/mixer.dir/mixer_simple.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "CONFIG_ARCH_BOARD_PX4_SITL"
  "ENABLE_LOCKSTEP_SCHEDULER"
  "__CUSTOM_FILE_IO__"
  "__DF_LINUX"
  "__PX4_LINUX"
  "__PX4_POSIX"
  "__STDC_FORMAT_MACROS"
  "noreturn_function=__attribute__((noreturn))"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../boards/px4/sitl/src"
  "."
  "src"
  "src/lib"
  "src/modules"
  "../src"
  "../src/include"
  "../src/lib"
  "../src/lib/DriverFramework/framework/include"
  "../src/lib/matrix"
  "../src/modules"
  "../src/platforms"
  "../src/platforms/common"
  "../platforms/posix/include"
  "external/Install/include"
  "src/lib/mixer"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")

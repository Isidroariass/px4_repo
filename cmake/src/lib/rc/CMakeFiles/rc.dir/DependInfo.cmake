# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/isidro/src/Firmware/src/lib/rc/common_rc.cpp" "/home/isidro/src/Firmware/cmake/src/lib/rc/CMakeFiles/rc.dir/common_rc.cpp.o"
  "/home/isidro/src/Firmware/src/lib/rc/crsf.cpp" "/home/isidro/src/Firmware/cmake/src/lib/rc/CMakeFiles/rc.dir/crsf.cpp.o"
  "/home/isidro/src/Firmware/src/lib/rc/dsm.cpp" "/home/isidro/src/Firmware/cmake/src/lib/rc/CMakeFiles/rc.dir/dsm.cpp.o"
  "/home/isidro/src/Firmware/src/lib/rc/sbus.cpp" "/home/isidro/src/Firmware/cmake/src/lib/rc/CMakeFiles/rc.dir/sbus.cpp.o"
  "/home/isidro/src/Firmware/src/lib/rc/st24.cpp" "/home/isidro/src/Firmware/cmake/src/lib/rc/CMakeFiles/rc.dir/st24.cpp.o"
  "/home/isidro/src/Firmware/src/lib/rc/sumd.cpp" "/home/isidro/src/Firmware/cmake/src/lib/rc/CMakeFiles/rc.dir/sumd.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "CONFIG_ARCH_BOARD_PX4_SITL"
  "ENABLE_LOCKSTEP_SCHEDULER"
  "__CUSTOM_FILE_IO__"
  "__DF_LINUX"
  "__PX4_LINUX"
  "__PX4_POSIX"
  "__STDC_FORMAT_MACROS"
  "noreturn_function=__attribute__((noreturn))"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../boards/px4/sitl/src"
  "."
  "src"
  "src/lib"
  "src/modules"
  "../src"
  "../src/include"
  "../src/lib"
  "../src/lib/DriverFramework/framework/include"
  "../src/lib/matrix"
  "../src/modules"
  "../src/platforms"
  "../src/platforms/common"
  "../platforms/posix/include"
  "external/Install/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")

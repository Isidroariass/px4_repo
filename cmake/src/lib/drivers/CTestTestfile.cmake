# CMake generated Testfile for 
# Source directory: /home/isidro/src/Firmware/src/lib/drivers
# Build directory: /home/isidro/src/Firmware/cmake/src/lib/drivers
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs(accelerometer)
subdirs(airspeed)
subdirs(device)
subdirs(gyroscope)
subdirs(led)
subdirs(linux_gpio)
subdirs(magnetometer)
subdirs(smbus)
subdirs(tone_alarm)

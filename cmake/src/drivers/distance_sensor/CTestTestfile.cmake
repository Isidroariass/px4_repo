# CMake generated Testfile for 
# Source directory: /home/isidro/src/Firmware/src/drivers/distance_sensor
# Build directory: /home/isidro/src/Firmware/cmake/src/drivers/distance_sensor
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs(cm8jl65)
subdirs(leddar_one)
subdirs(ll40ls)
subdirs(mb12xx)
subdirs(pga460)
subdirs(sf0x)
subdirs(sf1xx)
subdirs(srf02)
subdirs(teraranger)
subdirs(tfmini)
subdirs(ulanding)
subdirs(vl53lxx)

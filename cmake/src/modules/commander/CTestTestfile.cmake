# CMake generated Testfile for 
# Source directory: /home/isidro/src/Firmware/src/modules/commander
# Build directory: /home/isidro/src/Firmware/cmake/src/modules/commander
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs(failure_detector)
subdirs(commander_tests)

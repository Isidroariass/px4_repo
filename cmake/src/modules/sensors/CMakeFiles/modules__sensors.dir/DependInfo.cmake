# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/isidro/src/Firmware/src/modules/sensors/parameters.cpp" "/home/isidro/src/Firmware/cmake/src/modules/sensors/CMakeFiles/modules__sensors.dir/parameters.cpp.o"
  "/home/isidro/src/Firmware/src/modules/sensors/rc_update.cpp" "/home/isidro/src/Firmware/cmake/src/modules/sensors/CMakeFiles/modules__sensors.dir/rc_update.cpp.o"
  "/home/isidro/src/Firmware/src/modules/sensors/sensors.cpp" "/home/isidro/src/Firmware/cmake/src/modules/sensors/CMakeFiles/modules__sensors.dir/sensors.cpp.o"
  "/home/isidro/src/Firmware/src/modules/sensors/temperature_compensation.cpp" "/home/isidro/src/Firmware/cmake/src/modules/sensors/CMakeFiles/modules__sensors.dir/temperature_compensation.cpp.o"
  "/home/isidro/src/Firmware/src/modules/sensors/voted_sensors_update.cpp" "/home/isidro/src/Firmware/cmake/src/modules/sensors/CMakeFiles/modules__sensors.dir/voted_sensors_update.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "CONFIG_ARCH_BOARD_PX4_SITL"
  "ENABLE_LOCKSTEP_SCHEDULER"
  "MODULE_NAME=\"sensors\""
  "PX4_MAIN=sensors_app_main"
  "__CUSTOM_FILE_IO__"
  "__DF_LINUX"
  "__PX4_LINUX"
  "__PX4_POSIX"
  "__STDC_FORMAT_MACROS"
  "noreturn_function=__attribute__((noreturn))"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../boards/px4/sitl/src"
  "."
  "src"
  "src/lib"
  "src/modules"
  "../src"
  "../src/include"
  "../src/lib"
  "../src/lib/DriverFramework/framework/include"
  "../src/lib/matrix"
  "../src/modules"
  "../src/platforms"
  "../src/platforms/common"
  "../platforms/posix/include"
  "external/Install/include"
  "../src/lib/ecl"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/isidro/src/Firmware/cmake/platforms/posix/src/px4_layer/CMakeFiles/px4_layer.dir/DependInfo.cmake"
  "/home/isidro/src/Firmware/cmake/src/platforms/common/CMakeFiles/px4_platform.dir/DependInfo.cmake"
  "/home/isidro/src/Firmware/cmake/src/lib/systemlib/CMakeFiles/systemlib.dir/DependInfo.cmake"
  "/home/isidro/src/Firmware/cmake/src/lib/airspeed/CMakeFiles/airspeed.dir/DependInfo.cmake"
  "/home/isidro/src/Firmware/cmake/src/lib/battery/CMakeFiles/battery.dir/DependInfo.cmake"
  "/home/isidro/src/Firmware/cmake/src/lib/conversion/CMakeFiles/conversion.dir/DependInfo.cmake"
  "/home/isidro/src/Firmware/cmake/src/lib/drivers/device/CMakeFiles/drivers__device.dir/DependInfo.cmake"
  "/home/isidro/src/Firmware/cmake/src/lib/ecl/validation/CMakeFiles/ecl_validation.dir/DependInfo.cmake"
  "/home/isidro/src/Firmware/cmake/src/lib/mathlib/CMakeFiles/mathlib.dir/DependInfo.cmake"
  "/home/isidro/src/Firmware/cmake/platforms/posix/src/px4_daemon/CMakeFiles/px4_daemon.dir/DependInfo.cmake"
  "/home/isidro/src/Firmware/cmake/src/modules/uORB/CMakeFiles/modules__uORB.dir/DependInfo.cmake"
  "/home/isidro/src/Firmware/cmake/src/lib/cdev/CMakeFiles/cdev.dir/DependInfo.cmake"
  "/home/isidro/src/Firmware/cmake/src/platforms/common/work_queue/CMakeFiles/work_queue.dir/DependInfo.cmake"
  "/home/isidro/src/Firmware/cmake/platforms/posix/src/lockstep_scheduler/CMakeFiles/lockstep_scheduler.dir/DependInfo.cmake"
  "/home/isidro/src/Firmware/cmake/src/lib/parameters/CMakeFiles/parameters.dir/DependInfo.cmake"
  "/home/isidro/src/Firmware/cmake/src/lib/perf/CMakeFiles/perf.dir/DependInfo.cmake"
  "/home/isidro/src/Firmware/cmake/src/lib/parameters/tinybson/CMakeFiles/tinybson.dir/DependInfo.cmake"
  "/home/isidro/src/Firmware/cmake/msg/CMakeFiles/uorb_msgs.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")

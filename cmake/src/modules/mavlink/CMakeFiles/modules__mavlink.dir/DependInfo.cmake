# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/home/isidro/src/Firmware/src/modules/mavlink/mavlink.c" "/home/isidro/src/Firmware/cmake/src/modules/mavlink/CMakeFiles/modules__mavlink.dir/mavlink.c.o"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_C
  "CONFIG_ARCH_BOARD_PX4_SITL"
  "ENABLE_LOCKSTEP_SCHEDULER"
  "MODULE_NAME=\"mavlink\""
  "PX4_BOARD_NAME=\"PX4_SITL\""
  "PX4_MAIN=mavlink_app_main"
  "__CUSTOM_FILE_IO__"
  "__DF_LINUX"
  "__PX4_LINUX"
  "__PX4_POSIX"
  "__STDC_FORMAT_MACROS"
  "noreturn_function=__attribute__((noreturn))"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "../boards/px4/sitl/src"
  "."
  "src"
  "src/lib"
  "src/modules"
  "../src"
  "../src/include"
  "../src/lib"
  "../src/lib/DriverFramework/framework/include"
  "../src/lib/matrix"
  "../src/modules"
  "../src/platforms"
  "../src/platforms/common"
  "../platforms/posix/include"
  "external/Install/include"
  "../mavlink/include/mavlink"
  "../src/lib/ecl"
  )
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/isidro/src/Firmware/src/modules/mavlink/mavlink_command_sender.cpp" "/home/isidro/src/Firmware/cmake/src/modules/mavlink/CMakeFiles/modules__mavlink.dir/mavlink_command_sender.cpp.o"
  "/home/isidro/src/Firmware/src/modules/mavlink/mavlink_ftp.cpp" "/home/isidro/src/Firmware/cmake/src/modules/mavlink/CMakeFiles/modules__mavlink.dir/mavlink_ftp.cpp.o"
  "/home/isidro/src/Firmware/src/modules/mavlink/mavlink_high_latency2.cpp" "/home/isidro/src/Firmware/cmake/src/modules/mavlink/CMakeFiles/modules__mavlink.dir/mavlink_high_latency2.cpp.o"
  "/home/isidro/src/Firmware/src/modules/mavlink/mavlink_log_handler.cpp" "/home/isidro/src/Firmware/cmake/src/modules/mavlink/CMakeFiles/modules__mavlink.dir/mavlink_log_handler.cpp.o"
  "/home/isidro/src/Firmware/src/modules/mavlink/mavlink_main.cpp" "/home/isidro/src/Firmware/cmake/src/modules/mavlink/CMakeFiles/modules__mavlink.dir/mavlink_main.cpp.o"
  "/home/isidro/src/Firmware/src/modules/mavlink/mavlink_messages.cpp" "/home/isidro/src/Firmware/cmake/src/modules/mavlink/CMakeFiles/modules__mavlink.dir/mavlink_messages.cpp.o"
  "/home/isidro/src/Firmware/src/modules/mavlink/mavlink_mission.cpp" "/home/isidro/src/Firmware/cmake/src/modules/mavlink/CMakeFiles/modules__mavlink.dir/mavlink_mission.cpp.o"
  "/home/isidro/src/Firmware/src/modules/mavlink/mavlink_orb_subscription.cpp" "/home/isidro/src/Firmware/cmake/src/modules/mavlink/CMakeFiles/modules__mavlink.dir/mavlink_orb_subscription.cpp.o"
  "/home/isidro/src/Firmware/src/modules/mavlink/mavlink_parameters.cpp" "/home/isidro/src/Firmware/cmake/src/modules/mavlink/CMakeFiles/modules__mavlink.dir/mavlink_parameters.cpp.o"
  "/home/isidro/src/Firmware/src/modules/mavlink/mavlink_rate_limiter.cpp" "/home/isidro/src/Firmware/cmake/src/modules/mavlink/CMakeFiles/modules__mavlink.dir/mavlink_rate_limiter.cpp.o"
  "/home/isidro/src/Firmware/src/modules/mavlink/mavlink_receiver.cpp" "/home/isidro/src/Firmware/cmake/src/modules/mavlink/CMakeFiles/modules__mavlink.dir/mavlink_receiver.cpp.o"
  "/home/isidro/src/Firmware/src/modules/mavlink/mavlink_shell.cpp" "/home/isidro/src/Firmware/cmake/src/modules/mavlink/CMakeFiles/modules__mavlink.dir/mavlink_shell.cpp.o"
  "/home/isidro/src/Firmware/src/modules/mavlink/mavlink_simple_analyzer.cpp" "/home/isidro/src/Firmware/cmake/src/modules/mavlink/CMakeFiles/modules__mavlink.dir/mavlink_simple_analyzer.cpp.o"
  "/home/isidro/src/Firmware/src/modules/mavlink/mavlink_stream.cpp" "/home/isidro/src/Firmware/cmake/src/modules/mavlink/CMakeFiles/modules__mavlink.dir/mavlink_stream.cpp.o"
  "/home/isidro/src/Firmware/src/modules/mavlink/mavlink_timesync.cpp" "/home/isidro/src/Firmware/cmake/src/modules/mavlink/CMakeFiles/modules__mavlink.dir/mavlink_timesync.cpp.o"
  "/home/isidro/src/Firmware/src/modules/mavlink/mavlink_ulog.cpp" "/home/isidro/src/Firmware/cmake/src/modules/mavlink/CMakeFiles/modules__mavlink.dir/mavlink_ulog.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "CONFIG_ARCH_BOARD_PX4_SITL"
  "ENABLE_LOCKSTEP_SCHEDULER"
  "MODULE_NAME=\"mavlink\""
  "PX4_BOARD_NAME=\"PX4_SITL\""
  "PX4_MAIN=mavlink_app_main"
  "__CUSTOM_FILE_IO__"
  "__DF_LINUX"
  "__PX4_LINUX"
  "__PX4_POSIX"
  "__STDC_FORMAT_MACROS"
  "noreturn_function=__attribute__((noreturn))"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../boards/px4/sitl/src"
  "."
  "src"
  "src/lib"
  "src/modules"
  "../src"
  "../src/include"
  "../src/lib"
  "../src/lib/DriverFramework/framework/include"
  "../src/lib/matrix"
  "../src/modules"
  "../src/platforms"
  "../src/platforms/common"
  "../platforms/posix/include"
  "external/Install/include"
  "../mavlink/include/mavlink"
  "../src/lib/ecl"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/isidro/src/Firmware/cmake/platforms/posix/src/px4_layer/CMakeFiles/px4_layer.dir/DependInfo.cmake"
  "/home/isidro/src/Firmware/cmake/src/platforms/common/CMakeFiles/px4_platform.dir/DependInfo.cmake"
  "/home/isidro/src/Firmware/cmake/src/lib/systemlib/CMakeFiles/systemlib.dir/DependInfo.cmake"
  "/home/isidro/src/Firmware/cmake/src/lib/airspeed/CMakeFiles/airspeed.dir/DependInfo.cmake"
  "/home/isidro/src/Firmware/cmake/src/lib/conversion/CMakeFiles/conversion.dir/DependInfo.cmake"
  "/home/isidro/src/Firmware/cmake/src/lib/ecl/geo/CMakeFiles/ecl_geo.dir/DependInfo.cmake"
  "/home/isidro/src/Firmware/cmake/src/lib/version/CMakeFiles/version.dir/DependInfo.cmake"
  "/home/isidro/src/Firmware/cmake/boards/px4/sitl/src/CMakeFiles/drivers_board.dir/DependInfo.cmake"
  "/home/isidro/src/Firmware/cmake/platforms/posix/src/px4_daemon/CMakeFiles/px4_daemon.dir/DependInfo.cmake"
  "/home/isidro/src/Firmware/cmake/src/modules/uORB/CMakeFiles/modules__uORB.dir/DependInfo.cmake"
  "/home/isidro/src/Firmware/cmake/src/lib/cdev/CMakeFiles/cdev.dir/DependInfo.cmake"
  "/home/isidro/src/Firmware/cmake/src/platforms/common/work_queue/CMakeFiles/work_queue.dir/DependInfo.cmake"
  "/home/isidro/src/Firmware/cmake/platforms/posix/src/lockstep_scheduler/CMakeFiles/lockstep_scheduler.dir/DependInfo.cmake"
  "/home/isidro/src/Firmware/cmake/src/lib/parameters/CMakeFiles/parameters.dir/DependInfo.cmake"
  "/home/isidro/src/Firmware/cmake/src/lib/perf/CMakeFiles/perf.dir/DependInfo.cmake"
  "/home/isidro/src/Firmware/cmake/src/lib/parameters/tinybson/CMakeFiles/tinybson.dir/DependInfo.cmake"
  "/home/isidro/src/Firmware/cmake/msg/CMakeFiles/uorb_msgs.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")

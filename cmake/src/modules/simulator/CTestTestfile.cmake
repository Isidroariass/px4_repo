# CMake generated Testfile for 
# Source directory: /home/isidro/src/Firmware/src/modules/simulator
# Build directory: /home/isidro/src/Firmware/cmake/src/modules/simulator
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs(ledsim)
subdirs(accelsim)
subdirs(airspeedsim)
subdirs(barosim)
subdirs(gpssim)
subdirs(gyrosim)
